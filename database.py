import json
import pymysql

# with open('config.json') as json_data_file:
    # cfg = json.load(json_data_file)
with open('config.json') as json_data_file:
    cfg = json.load(json_data_file)

class MySQL:
    def __init__(self):
        self.connection = None
        self.cursor = None

    def connect(self):
        self.connection = pymysql.connect(
            user=cfg['db_uid'],
            password=cfg['db_pwd'],
            host=cfg['db_host'],
            database=cfg['db_name'],
            charset='utf8')
        self.cursor = self.connection.cursor()

    def get_connection(self):
        return self.connection

    def get_cursor(self):
        return self.cursor
