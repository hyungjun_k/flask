from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class CreateUser(Resource):
    def get(self):
        return "Hello Get World!"
    def post(self):
        return "Hello Post World!"


api.add_resource(CreateUser, '/user')

if __name__ == '__main__':
    app.run(port=5000, debug=True)
